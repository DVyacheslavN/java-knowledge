import java.util.Random;

public class SequentialRandom {
    private static int random = (int) (Math.random() * (100 - 0 + 1));

    public SequentialRandom()
    {
        random++;
        System.out.println("random:" + random);
    }

    public int getRandom()
    {
        return random;
    }

    static public void resetRandom()
    {
        random = (int) (Math.random() * (100 - 0 + 1));
    }
}
