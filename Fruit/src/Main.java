public class Main {
    public static void main(String[] args) {
//        new SequentialRandom();
//        new SequentialRandom();
//        new SequentialRandom();
//        new SequentialRandom();
//        SequentialRandom.resetRandom();
//        new SequentialRandom();
//        new SequentialRandom();
//        new SequentialRandom();
//        new SequentialRandom();

        for (int i = 0; i < 5; i++) {
            Fruit fruit = createFruit();
            fruit.stringInfo();
        }
    }

    static Fruit createFruit()
    {
        int randomFruit = (int) (Math.random() * (2 - 0 + 1));

        switch (randomFruit) {
            case 0:
                return new Apple();
            case 1:
                return new Orange();
            case 2:
                return new Raspberry();
            default:
                return new Apple();
        }
    }
}
