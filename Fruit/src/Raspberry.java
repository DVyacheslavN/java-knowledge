public class Raspberry extends Fruit {
    String fruitName = "Raspberry";
    int seedsCount = (int) (Math.random() * (100 - 0 + 1));

    @Override
    public void stringInfo()
    {
        System.out.println("name:" + fruitName + " weight:" + weight + " seedsCount:" + seedsCount);
    }
}
