import kotlin.Pair;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

public class UrlModel {

    UrlModel(BufferedWriter writer_) throws IOException {
        writer = writer_;
    }

    private AtomicInteger readCounter = new AtomicInteger(0);
    private AtomicInteger writeCounter = new AtomicInteger(0);
    private Vector<String> websites = new Vector<String>();
    private final BufferedWriter writer;
    private final String value = "446f6d6e696e20562e4e";

    String begin = "===== НАЧАЛО САЙТА <%s> ======";
    String end = "===== КОНЕЦ САЙТА <%s> =<%s>=====";

    public void add(String str) {
        websites.add(str);
    }

    public Pair<Integer, String> getNext() {
        while (true) {
            var counter = readCounter.get();
            int newCounter = counter + 1;
            if (readCounter.compareAndSet(counter, newCounter)) {
                if (counter > websites.size() - 1) {
                    return new Pair<Integer, String>(-1, "");
                } else {
                    return new Pair<Integer, String>(counter, websites.get(counter));
                }
            }
        }
    }

    public void writeByIndex(Integer index, List<String> list) {
        var counter = index;
        int newCounter = counter + 1;
        String decodedStr = null;
        if (counter != websites.size() - 1) {
            byte[] bytes = new byte[0];
            try {
                bytes = Hex.decodeHex(value.toCharArray());
                decodedStr = String.format(end, websites.get(index), new String(bytes, "UTF-8"));
            } catch (UnsupportedEncodingException | DecoderException e) {
                e.printStackTrace();
            }
        } else {
            decodedStr = String.format(end, websites.get(index), "");
        }

        while (writeCounter.get() != newCounter) {
            if (writeCounter.get() == counter) {
                try {
                    writer.write(String.format(begin, websites.get(index)));
                    writer.newLine();
                    list.forEach(s -> {
                        try {
                            writer.write(s.toString());
                            writer.newLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                    writer.newLine();
                    writer.write(decodedStr);
                    writer.newLine();
                } catch (IOException e) {

                }
                writeCounter.compareAndSet(counter, newCounter);
            }
        }
    }

    public List<String> readOne(Integer index) {
        try {
            URL url = new URL(websites.get(index));
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            List<String> list = reader.lines().collect(Collectors.toList());
            reader.close();
            return list;
        } catch (IOException ex) {
            return null;
        }
    }
}
