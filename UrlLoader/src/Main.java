import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;

import java.net.*;
import java.io.*;

import java.lang.Object;
import java.net.URL;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello world");
        try {
            BufferedWriter wirter = new BufferedWriter(new FileWriter("sequential.txt"));
            UrlModel model = new UrlModel(wirter);
            model.add("https://javastudy.ru/interview/exceptions/");
            model.add("https://habr.com/ru/post/155467/");
            model.add("https://www.grammaring.com/the-use-of-the-infinitive");
            model.add("https://englex.ru/participle-in-english/");

            var ts1 = System.currentTimeMillis();
            Runnable run = () -> {
                String threadTitle = "===== Task %s is %s =====";
                System.out.println(String.format(threadTitle, Thread.currentThread().getName(), "running"));
                while (true) {
                    var url = model.getNext();
                    if (url.component1() == -1) {
                        break;
                    }
                    model.writeByIndex(url.component1(), model.readOne(url.component1()));
                }
                System.out.println(String.format(threadTitle, Thread.currentThread().getName(), "finished"));
            };

            Thread thread1 = new Thread(run, "thread1");
            Thread thread3 = new Thread(run, "thread2");
            Thread thread2 = new Thread(run, "thread3");
            Thread thread4 = new Thread(run, "thread4");

            thread1.start();
            thread2.start();
            thread3.start();
            thread4.start();

            thread1.join();
            thread2.join();
            thread3.join();
            thread4.join();

            wirter.close();
            var ts2 = System.currentTimeMillis();
            Path path = Paths.get("sequential.txt");
            System.out.println("time:" + (ts2 - ts1) + " size:" + Files.size(path));
        } catch (IOException | InterruptedException e) {

        }
    }
}
