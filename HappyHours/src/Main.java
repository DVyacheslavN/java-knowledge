import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<FixPriceShop> shops = new ArrayList<FixPriceShop>();

        for (int i = 0; i < 3; i++) {
            shops.add(new FixPriceShop());
        }

        shops.get(0).addItems(new String[]{"Морковь", "Капуста"});
        shops.get(1).addItems(new String[]{"Бананы", "Хлеб"});
        shops.get(2).addItems(new String[]{"Шоколад", "Торты"});

        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Select Shop number 1-3 or exit");
            String inputString = scanner.nextLine();
            if (inputString.compareTo("exit") == 0
                    || inputString.compareTo("q") == 0) {
                return;
            }
            var selectedShop = shops.get(Integer.parseInt(inputString) - 1);

            System.out.println("Type the item");
            String selectedItem = scanner.nextLine();
            int currentHour = (int) (Math.random() * (23 - 0 + 1));
            int price = selectedShop.checkItemPrice(selectedItem, currentHour);
            if (price == -1) {
                System.out.println(selectedItem + " doesn't exist");
                continue;
            }

            System.out.println("Current hour =" + currentHour);
            System.out.println("do you want to buy the item with price:" + price);
            inputString = scanner.nextLine();

            if (inputString.compareTo("yes") == 0
                    || inputString.compareTo("Yes") == 0
                    || inputString.compareTo("y") == 0) {
                selectedShop.buyItem(selectedItem, currentHour);
            }
        }
    }
}
