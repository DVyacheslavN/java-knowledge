import java.util.ArrayList;

public class FixPriceShop {
    FixPriceShop(){
        happyHour = (int) (Math.random() * (23 - 0 + 1));
        System.out.println("HappyHour is " + happyHour);
    }

    int happyHour = 0;
    static int fixedPrice = 50;
    ArrayList<String> items = new ArrayList<String>();

    void addItems(String[] args)
    {
        for (var arg : args) {
            items.add(arg);
        }
    }

    ArrayList<String> getItems() {
        return items;
    }

    int checkItemPrice(String item, int hour)
    {
        if (items.indexOf(item) == -1) {
            return -1;
        }

        if (happyHour == hour) {
            return fixedPrice / 2;
        }
        return fixedPrice;
    }

    void buyItem(String item, int hour)
    {
        String str = "товар <%s> продан по цене <%s>";
        int index = items.indexOf(item);

        if (index == -1)
        {
            String unknownItem =  "товар <%s> не обнаружен";
            System.out.println(String.format(unknownItem, item));
            return;
        }

        System.out.println(String.format(str, item, checkItemPrice(item, hour)));
        items.remove(index);
    }
}
