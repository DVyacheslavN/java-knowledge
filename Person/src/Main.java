import java.util.*;

public class Main {

    public static void main(String[] args) {
        ArrayList<Person> personList = new ArrayList<Person>(0);
        personList.add(new Person("AAA", 12));
        personList.add(new Person("BBB", 10));
        personList.add(new Person("DD", 18));
        personList.add(new Person("GG", 7));
        personList.add(new Person("NNNN", 14));

        Thread thread = new Thread(() -> {
            Collections.sort(personList, new SortByName());

            for (int i = 0; i < personList.size(); i++) {
                System.out.print(personList.get(i).m_name);
            }
        });


        thread.start();
    }
}
