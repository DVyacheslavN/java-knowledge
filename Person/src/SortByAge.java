import java.util.Comparator;

public class SortByAge implements Comparator<Person>{
        public int compare(Person a, Person b)
        {
            return a.m_age - b.m_age;
        }
}
