import java.util.Comparator;

public class SortByName implements Comparator<Person> {
    public int compare(Person a, Person b)
    {
        return a.m_name.compareTo(b.m_name) ;
    }
}