public class Circle extends Figure {
    public int radius = (int)(Math.random() * 20);

    @Override
    public int square() {
        return  (int)Math.PI * (radius * radius);
    }
}
