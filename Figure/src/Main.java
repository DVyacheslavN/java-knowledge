import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Figure> figures = new ArrayList<Figure>();
        figures.add(new Circle());
        figures.add(new Square());
        figures.add(new IsoscelesTriangle());
        figures.add(new Square());
        figures.add(new IsoscelesTriangle());

        for (var figure : figures)
        {
            System.out.println("Class:" + figure.getClass() + "square:" + figure.square());
        }
    }
}
