public class Square extends Figure {
    public int length = (int)((Math.random() * 20) + 70);

    @Override
    public int square() {
        return (int) Math.pow(length, 2);
    }
}
