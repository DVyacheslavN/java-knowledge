import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        List<Student> students = new ArrayList<>();
        students.add(new Student("Вася", Set.of(
                new Lection(Lection.LectionName.Нhysics.str(), LocalDate.of(10, 11, 12))
                , new Lection(Lection.LectionName.Mathematics.str(), LocalDate.of(10, 11, 12))
                , new Lection(Lection.LectionName.Engish.str(), LocalDate.of(11, 01, 12))
        )));

        students.add(new Student("Серей", Set.of(
                new Lection(Lection.LectionName.Philosophy.str(), LocalDate.of(10, 3, 1))
                , new Lection(Lection.LectionName.Philosophy.str(), LocalDate.of(10, 10, 15))
                , new Lection(Lection.LectionName.Engish.str(), LocalDate.of(11, 8, 25))
        )));

        students.add(new Student("Евгений", Set.of(
                new Lection(Lection.LectionName.Engish.str(), LocalDate.of(10, 6, 22))
                , new Lection(Lection.LectionName.Mathematics.str(), LocalDate.of(10, 5, 1))
                , new Lection(Lection.LectionName.Mathematics.str(), LocalDate.of(11, 8, 17))
        )));

        students.add(new Student("Олег", Set.of(
                new Lection(Lection.LectionName.Нistory.str(), LocalDate.of(10, 2, 3))
                , new Lection(Lection.LectionName.Philosophy.str(), LocalDate.of(10, 4, 12))
                , new Lection(Lection.LectionName.Mathematics.str(), LocalDate.of(11, 3, 11))
        )));

        students.add(new Student("Иван", Set.of(
                new Lection(Lection.LectionName.Philosophy.str(), LocalDate.of(10, 5, 24))
                , new Lection(Lection.LectionName.Engish.str(), LocalDate.of(10, 6, 26))
                , new Lection(Lection.LectionName.Physical.str(), LocalDate.of(11, 4, 28))
        )));

        students.stream().filter(student -> student.lections.stream().filter(
                lection -> lection.name == Lection.LectionName.Mathematics.str()).count() != 0).map(student -> student.name).forEach(System.out::println);

        System.out.println("XXXX");
        students.stream().forEach(student -> {
            System.out.println(student.name + " " + student.lections.stream().count());}
        );

        System.out.println("AAAAA");
        var cllect = students.stream().flatMap(student -> student.lections.stream().map(lection -> lection.name))
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        var max = Collections.max(cllect.values());
        cllect.forEach((s, aLong) -> {
            if (aLong == max)
            {
                System.out.println(s +" " + aLong);
            }
        });

        System.out.println("BBBBB");
        students.stream().collect(Collectors.toMap(student -> student.name, student -> student.lections.stream()
                .collect(Collectors.groupingBy(lection -> lection.date, Collectors.counting())).values().stream().max((s1, s2) -> s1.intValue() - s2.intValue())
        )).forEach((s, aLong) -> System.out.println(s + " :" + aLong.get()));

        System.out.println("CCCCC");

        students.stream().flatMap(student -> student.lections.stream().map(lection -> lection.name).distinct())
                .collect(Collectors.groupingBy(s -> s)).values().stream().forEach(strings -> System.out.println(strings.get(0) + " " + strings.size()));

    }
}
